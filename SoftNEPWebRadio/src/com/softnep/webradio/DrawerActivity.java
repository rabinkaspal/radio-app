package com.softnep.webradio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class DrawerActivity extends Activity {

	public DrawerLayout mDrawerLayout;
	public LinearLayout mDrawerList;
	public ActionBarDrawerToggle mDrawerToggle;

	public CharSequence mDrawerTitle;

	public CharSequence mTitle;

	public String[] navMenuTitles;
	public TypedArray navMenuIcons;

	public ViewGroup.LayoutParams params;

	// Button btnDraw;
	ImageView info_link, btnCloseDrawer;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		// setContentView(R.layout.drawer_layout);

		// btnDraw = (Button) findViewById(R.id.btnDraw);

		btnCloseDrawer = (ImageView) findViewById(R.id.btnCloseDrawer);

		info_link = (ImageView) findViewById(R.id.info_link);

		mTitle = mDrawerTitle = getTitle();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (LinearLayout) findViewById(R.id.list_slidermenu);

		mDrawerList.requestFocus();

		params = (ViewGroup.LayoutParams) mDrawerList.getLayoutParams();
		
		
		int deviceWidth = getResources().getDisplayMetrics().widthPixels; 
		if(deviceWidth <= 320){
			params.width = getResources().getDisplayMetrics().widthPixels * 3 / 4;
		}else if(deviceWidth > 640){
			params.width = 500;
		}else if(deviceWidth <= 640 || deviceWidth > 480){
			params.width = getResources().getDisplayMetrics().widthPixels * 4/5;
		}else{
			params.width = 380;
		}
		mDrawerList.setLayoutParams(params);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.sn_minus, R.string.app_name, R.string.app_name) {

			public void onDrawerClosed(View drawerView) {
				// do nothing
			};

			public void onDrawerOpened(View drawerView) {
				// do nothing
			};
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		// mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		info_link.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mDrawerLayout.openDrawer(Gravity.END);
			}
		});

		btnCloseDrawer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mDrawerLayout.closeDrawer(Gravity.END);
			}
		});

	}
	

	// @SuppressLint("NewApi")
	// @Override
	// public void setTitle(CharSequence title) {
	// mTitle = title;
	// getActionBar().setTitle(mTitle);
	// }

	// @Override
	// protected void onPostCreate(Bundle savedInstanceState) {
	// super.onPostCreate(savedInstanceState);
	// mDrawerToggle.syncState();
	// }

	// @Override
	// public void onConfigurationChanged(Configuration newConfig) {
	// // TODO Auto-generated method stub
	// super.onConfigurationChanged(newConfig);
	// mDrawerToggle.onConfigurationChanged(newConfig);
	// }

}
