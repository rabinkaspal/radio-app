package com.softnep.webradio;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionHelper {

	public static boolean isConnected = false;
	
	
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						if (!isConnected) {
							isConnected = true;
							// TODO
						}
						return true;
					}
				}
			}
		}
		isConnected = false;
		return false;
	}
	
	

	public static AlertDialog.Builder buildDialog(Context c) {

	    AlertDialog.Builder builder = new AlertDialog.Builder(c);
	    builder.setTitle("No Internet connection.");
	    builder.setMessage("You have no internet connection");

	    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	            dialog.dismiss();
	        }
	    });

	    return builder;
	}
	
}
