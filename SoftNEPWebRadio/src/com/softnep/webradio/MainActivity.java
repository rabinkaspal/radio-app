package com.softnep.webradio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

public class MainActivity extends DrawerActivity implements OnClickListener {

	private static final String LOG = "SOFTNEP log";

	private static MultiPlayer multiPlayer;
	private static Handler uiHandler;
	private static TextView txtStatus;
	// txtMetaTitle, txtMetaUrl, txtMetaGenre;
	public static boolean playerStarted;
	public static boolean wasPlayingBeforePhoneCall = false;
	public static boolean wasPlayingBeforeAudioFocusLoss = false;
	public static boolean isStarted = false;

	public static int result;

	// "http://streaming.softnep.net:8005/http://kantipur-stream.softnep.com:7248/";
	private final static String URL_RADIO_SOFTNEP = "http://streaming.softnep.net:8005/";

	NotificationActionButtonReceiver bReceiver = new NotificationActionButtonReceiver();

	public static RemoteViews notification_view;

	public static Context context = null;

	public static ImageView btnPlay, btnPause, btnVolumeDown, btnVolumeUp,
			sn_social_fb, sn_social_tw, sn_social_gp, sn_social_ln;

	boolean volumeUpTouching = false;
	boolean volumeDownTouching = false;

	private TextView moto, contact, emailLabel, email, phoneLabel, phone,
			website, tv1, tv2, tv3, tv4, tv5;

	private static ProgressBar progressBar;

	// public static MediaPlayer player;

	private SeekBar volumeSeekbar = null;
	private static AudioManager audioManager = null;

	public static NotificationManager mNotificationManager;
	private static Notification notification;

	private PhoneStateListener phoneStateListener;
	private TelephonyManager mgr;
	private static OnAudioFocusChangeListener audioFocusChangeListener;

	LinearLayout seekbar_layout;

	private Vibrator myVib;

	public static ViewGroup.LayoutParams params;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
		super.onCreate(savedInstanceState);
		context = this;
		// runTask();

		initializeUIComponents();
		startService(new Intent(this, KillNotificationsService.class));
		// if (!isStarted)
		// start();

		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		
		
		audioFocusChangeListener = new OnAudioFocusChangeListener() {
			
			@Override
			public void onAudioFocusChange(int focusChange) {
				switch (focusChange) {
				case AudioManager.AUDIOFOCUS_GAIN:

					Log.d("FOCUS", "focus gained now");

					// resume playback
					// wasPlayingBeforeAudioFocusLoss

					// if (wasPlayingBeforeAudioFocusLoss) {
					// Toast.makeText(getApplicationContext(),
					// "audio focussed gained", Toast.LENGTH_SHORT)
					// .show();
					if (!isStarted && wasPlayingBeforePhoneCall)
						start();
					// }
					break;

				case AudioManager.AUDIOFOCUS_LOSS:

//					Toast.makeText(getApplicationContext(),
//							"focus lost for unbounded time now",
//							Toast.LENGTH_SHORT).show();

					// Log.d("FOCUS",
					// "focus lost for unbounded time now");
					// Lost focus for an unbounded amount of time: stop
					// playback
					// wasPlayingBeforeAudioFocusLoss = playerStarted;
					stop();
					removeNotification();
					closeActivity();
					break;

				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//					Toast.makeText(getApplicationContext(),
//							"focus lost temporarily",
//							Toast.LENGTH_SHORT).show();

					// Log.d("FOCUS", "focus lost temporarily.");
					// probably a phone call and media plays
					// Lost focus for a short time, but we have to stop
					// playback. We don't release the media player
					// because playback
					// is likely to resume
					// wasPlayingBeforeAudioFocusLoss = playerStarted;
					stop();
					removeNotification();
					break;

				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
				// Log.d("FOCUS",
				// "focus lost transiently.. volume decreasing");
				// // Lost focus for a short time, but it's ok to keep
				// // playing
				// // at an attenuated level
					break;
				}

			}
		};
		
		result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

		if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
//			Toast.makeText(getApplicationContext(),
//					"audio focus g", Toast.LENGTH_SHORT)
//					.show();
			if (!isStarted)
				start();
		} else if (result == AudioManager.AUDIOFOCUS_REQUEST_FAILED){
			// audio focus cannot be granted. cannot play audio
			progressBar.setVisibility(View.GONE);
			btnPlay.setVisibility(View.VISIBLE);
			btnPlay.setEnabled(true);
			Toast.makeText(getApplicationContext(),
					"Cant play radio now. Try again.", Toast.LENGTH_SHORT)
					.show();
		}

		// handle play/pause of radio on phone calls

		phoneStateListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				if (state == TelephonyManager.CALL_STATE_RINGING) {
					// Incoming call: Pause music
					wasPlayingBeforePhoneCall = playerStarted;
					stop();

				} else if (state == TelephonyManager.CALL_STATE_IDLE) {
					// Not in call: Play music
					if (wasPlayingBeforePhoneCall) {

					}
				} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
					// A call is dialing, active or on hold
					wasPlayingBeforePhoneCall = playerStarted;
					stop();
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};

		mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if (mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

		}

	}

	public static void runTask() {
		if (ConnectionHelper
				.isNetworkAvailable(context.getApplicationContext())) {
//			isOnline = true;
		} else {
//			isOnline = false;
			CustomDialog cd = new CustomDialog((Activity) context);
			cd.show();
		}

	}

	private void browse(String url) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.addCategory(Intent.CATEGORY_BROWSABLE);
		intent.setData(Uri.parse(url));
		startActivity(intent);
	}

	public static void closeActivity() {
		((Activity) context).finish();
		System.exit(0);
	}
	public static void exit(){
		System.exit(0);
	}

	private void initializeUIComponents() {

		txtStatus = (TextView) findViewById(R.id.txtStatus);
		// txtMetaTitle = (TextView) findViewById(R.id.txtMetaTitle);
		// txtMetaUrl = (TextView) findViewById(R.id.txtMetaUrl);
		// txtMetaGenre = (TextView) findViewById(R.id.txtMetaGenre);

		uiHandler = new Handler();

		myVib = (Vibrator) getSystemService(VIBRATOR_SERVICE);

		seekbar_layout = (LinearLayout) findViewById(R.id.seekbar_layout);

		btnPlay = (ImageView) findViewById(R.id.btnPlay);
		btnPlay.setOnClickListener(this);

		btnPause = (ImageView) findViewById(R.id.btnPause);
		btnPause.setOnClickListener(this);
		btnPause.setVisibility(View.GONE);

		contact = (TextView) findViewById(R.id.contact);
		emailLabel = (TextView) findViewById(R.id.emailLabel);
		email = (TextView) findViewById(R.id.email);
		phoneLabel = (TextView) findViewById(R.id.phoneLabel);
		phone = (TextView) findViewById(R.id.phone);
		website = (TextView) findViewById(R.id.website);
		moto = (TextView) findViewById(R.id.moto);
		tv1 = (TextView) findViewById(R.id.tv1);
		tv2 = (TextView) findViewById(R.id.tv2);
		tv3 = (TextView) findViewById(R.id.tv3);
		tv4 = (TextView) findViewById(R.id.tv4);
		tv5 = (TextView) findViewById(R.id.tv5);

		Typeface openSansSemiBold = Typeface.createFromAsset(getAssets(),
				"fonts/OPENSANS-SEMIBOLD.TTF");
		Typeface openSansBold = Typeface.createFromAsset(getAssets(),
				"fonts/OPENSANS-BOLD.TTF");
		Typeface openSansRegular = Typeface.createFromAsset(getAssets(),
				"fonts/OPENSANS-REGULAR.TTF");

		moto.setTypeface(openSansBold);
		contact.setTypeface(openSansSemiBold);
		emailLabel.setTypeface(openSansSemiBold);
		email.setTypeface(openSansSemiBold);
		phoneLabel.setTypeface(openSansSemiBold);
		phone.setTypeface(openSansSemiBold);
		website.setTypeface(openSansSemiBold);
		tv1.setTypeface(openSansBold);
		tv3.setTypeface(openSansBold);
		tv5.setTypeface(openSansBold);
		tv2.setTypeface(openSansRegular);
		tv4.setTypeface(openSansRegular);

		email.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent sendIntent = new Intent(Intent.ACTION_VIEW);
				sendIntent.setClassName("com.google.android.gm",
						"com.google.android.gm.ComposeActivityGmail");
				sendIntent.putExtra(Intent.EXTRA_EMAIL,
						new String[] { "mkt@softnep.com" });
				sendIntent.setData(Uri.parse("mkt@softnep.com"));
				sendIntent.putExtra(Intent.EXTRA_SUBJECT, "SoftNEP Radio");
				sendIntent.setType("plain/text");
				sendIntent.putExtra(Intent.EXTRA_TEXT, "Type content here...");
				startActivity(sendIntent);
			}
		});

		website.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Uri url = Uri.parse("http://www.softnep.com");
				Intent i = new Intent(Intent.ACTION_VIEW, url);
				startActivity(i);
			}
		});

//		phone.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// if (prepared == 0) {
//				// Toast.makeText(getApplicationContext(),
//				// "Cant make calls while preparing player",
//				// Toast.LENGTH_SHORT).show();
//				// } else {
//				String phone_no = phone.getText().toString()
//						.replaceAll("-", "");
//				Intent callIntent = new Intent(Intent.ACTION_CALL);
//				callIntent.setData(Uri.parse("tel:" + phone_no));
//				callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//				startActivity(callIntent);
//				// }
//			}
//		});

		sn_social_fb = (ImageView) findViewById(R.id.iv_social_fb);
		sn_social_fb.setOnClickListener(this);

		sn_social_tw = (ImageView) findViewById(R.id.iv_social_tw);
		sn_social_tw.setOnClickListener(this);

		sn_social_gp = (ImageView) findViewById(R.id.iv_social_gp);
		sn_social_gp.setOnClickListener(this);

		sn_social_ln = (ImageView) findViewById(R.id.iv_social_ln);
		sn_social_ln.setOnClickListener(this);

		btnVolumeUp = (ImageView) findViewById(R.id.btnVolumeUp);
		btnVolumeUp.setOnClickListener(this);

		btnVolumeUp.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				volumeUpTouching = true;
				Thread volumeUpThread = new Thread() {
					public void run() {
						while (volumeUpTouching) {
							int index = volumeSeekbar.getProgress();
							volumeSeekbar.setProgress(index + 1);
							try {
								sleep(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				};
				volumeUpThread.start();
				return false;
			}
		});

		btnVolumeDown = (ImageView) findViewById(R.id.btnVolumeDown);
		btnVolumeDown.setOnClickListener(this);

		btnVolumeDown.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				volumeDownTouching = true;
				Thread volumeDownThread = new Thread() {
					public void run() {
						while (volumeDownTouching) {
							int index = volumeSeekbar.getProgress();
							volumeSeekbar.setProgress(index - 1);
							try {
								sleep(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				};
				volumeDownThread.start();
				return false;
			}
		});

		progressBar = (ProgressBar) findViewById(R.id.progressBarCircular);
		progressBar.setVisibility(View.GONE);

		volumeSeekbar = (SeekBar) findViewById(R.id.volumeSeekbar);
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		volumeSeekbar.setMax(audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		volumeSeekbar.setProgress(audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC));

		volumeSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress,
					boolean arg2) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						progress, 0);
			}
		});

		// /converting icy headers to http
		try {
			java.net.URL
					.setURLStreamHandlerFactory(new java.net.URLStreamHandlerFactory() {
						public java.net.URLStreamHandler createURLStreamHandler(
								String protocol) {
							Log.d(LOG,
									"Asking for stream handler for protocol: '"
											+ protocol + "'");
							if ("icy".equals(protocol))
								return new com.spoledge.aacdecoder.IcyURLStreamHandler();
							return null;
						}
					});
		} catch (Throwable t) {
			Log.w(LOG,
					"Cannot set the ICY URLStreamHandler - maybe already set ? - "
							+ t);
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			int index = volumeSeekbar.getProgress();
			volumeSeekbar.setProgress(index + 1);
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			int index = volumeSeekbar.getProgress();
			volumeSeekbar.setProgress(index - 1);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	int backClickCount = 0;
	
	
	@Override
	protected void onResume() {
		Log.d("onResume", "onResume called");
		super.onResume();
	}
	
	@Override
	protected void onPause() {
	Log.d("onPause", "onPaused called");
	super.onPause();
	}
	
	@Override
	protected void onStop() {
	Log.d("onStop", "onStop called");
	super.onStop();
	}

	@Override
	public void onBackPressed() {
		backClickCount++;
		if (playerStarted) {
			Toast.makeText(getApplicationContext(),
					"SoftNEP Radio now playing in background.", Toast.LENGTH_SHORT)
					.show();
			moveTaskToBack(true);
		} else {

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					backClickCount = 0;
				}
			}, 1500);

			if (backClickCount == 1) {
				Toast.makeText(getApplicationContext(),
						"Press back again to exit.", Toast.LENGTH_SHORT).show();
			} else {
				backClickCount = 0;
				 closeActivity();
//				moveTaskToBack(false);
//				System.exit(0);
				 super.onBackPressed();
			}

		}
		// else {
		// if (prepared == 0) {
		// Log.d("MP", "exiting \n mp not prepared. no cleaning required");
		// //player.release();
		// player = null;
		// }
		// Log.d("MP",
		// "exiting app.\n may be preparing OR (prepared and paused) \n releasing player.");
		// if(player!= null){
		// player.stop();
		// //player.release();
		// player = null;}
		// paused = 0;
		// super.onBackPressed();

		// }
	}

	public void onClick(View v) {

		if (v == btnPlay) {
			// startPlaying();
			start();
		} else if (v == btnPause) {
			// pausePlaying();
			stop();
		} else if (v == btnVolumeDown) {
			int index = volumeSeekbar.getProgress();
			volumeSeekbar.setProgress(index - 1);
			volumeDownTouching = false;
		} else if (v == btnVolumeUp) {
			int index = volumeSeekbar.getProgress();
			volumeSeekbar.setProgress(index + 1);
			volumeUpTouching = false;
		} else if (v == sn_social_fb) {
			myVib.vibrate(100);
			browse("http://www.facebook.com/softnep");
		} else if (v == sn_social_tw) {
			myVib.vibrate(100);
			browse("http://www.twitter.com/softnep");
		} else if (v == sn_social_gp) {
			myVib.vibrate(100);
			browse("http://www.plus.google.com/softnep");
		} else if (v == sn_social_ln) {
			myVib.vibrate(100);
			browse("http://www.linkedin.com/softnep");
		} else if (v == email) {
			myVib.vibrate(100);
		} else if (v == phone) {

		}

	}

	@Override
	protected void onDestroy() {

		if (multiPlayer != null && playerStarted) {
			removeNotification();
			stop();
		}

		if (mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
		}

		Log.d("MP", "destroying app");

		if (audioManager != null) {
			audioManager = null;
		}
		super.onDestroy();
	}

	static PlayerCallback mPlayerCallback = new PlayerCallback() {

		public void playerStarted() {
			uiHandler.post(new Runnable() {
				public void run() {
					// txtBufAudio.setEnabled( false );
					// txtBufDecode.setEnabled( false );
					// btnPlay.setEnabled(false);
					// btnPause.setEnabled(true);

					btnPlay.setVisibility(View.GONE);
					btnPause.setVisibility(View.VISIBLE);
					progressBar.setVisibility(View.GONE);
					playerStarted = true;
					setNotification();
				}
			});
		}

		/**
		 * This method is called periodically by PCMFeed.
		 * 
		 * @param isPlaying
		 *            false means that the PCM data are being buffered, but the
		 *            audio is not playing yet
		 * 
		 * @param audioBufferSizeMs
		 *            the buffered audio data expressed in milliseconds of
		 *            playing
		 * @param audioBufferCapacityMs
		 *            the total capacity of audio buffer expressed in
		 *            milliseconds of playing
		 */
		public void playerPCMFeedBuffer(final boolean isPlaying,
				final int audioBufferSizeMs, final int audioBufferCapacityMs) {

			uiHandler.post(new Runnable() {
				public void run() {
					// progress.setProgress(audioBufferSizeMs *
					// progress.getMax()
					// / audioBufferCapacityMs);
					if (isPlaying) {
						txtStatus.setText("On Air");
					}
				}
			});
		}

		public void playerStopped(final int perf) {
			uiHandler.post(new Runnable() {
				public void run() {
					// btnPlay.setEnabled(true);
					// btnPause.setEnabled(false);
					btnPlay.setVisibility(View.VISIBLE);
					btnPause.setVisibility(View.GONE);
					// txtBufAudio.setEnabled( true );
					// txtBufDecode.setEnabled( true );
					// txtStatus.setText( R.string.text_stopped );
					// txtStatus.setText("" + perf + " %");
					// progress.setVisibility(View.INVISIBLE);
					playerStarted = false;
					txtStatus.setText("Stopped");
					// txtMetaTitle.setText("");
					// txtMetaGenre.setText("");
					// txtMetaUrl.setText("");
					removeNotification();
				}
			});
		}

		public void playerException(final Throwable t) {
			uiHandler.post(new Runnable() {
				public void run() {
					// new AlertDialog.Builder(MainActivity.this)
					// .setTitle("Error")
					// .setMessage(t.toString())
					// .setNeutralButton("Close",
					// new DialogInterface.OnClickListener() {
					// public void onClick(
					// DialogInterface dialog, int id) {
					// dialog.cancel();
					// }
					// }).show();

					txtStatus.setText("Radio is off.");
					btnPlay.setVisibility(View.VISIBLE);
					progressBar.setVisibility(View.GONE);

					if (playerStarted)
						playerStopped(0);
				}
			});
		}

		public void playerMetadata(final String key, final String value) {
			TextView tv = null;

			if ("StreamTitle".equals(key) || "icy-name".equals(key)
					|| "icy-description".equals(key)) {
				// tv = txtMetaTitle;
			} else if ("StreamUrl".equals(key) || "icy-url".equals(key)) {
				// tv = txtMetaUrl;
			} else if ("icy-genre".equals(key)) {
				// tv = txtMetaGenre;
			} else
				return;

			final TextView ftv = tv;

			uiHandler.post(new Runnable() {
				public void run() {
					// ftv.setText(value);
				}
			});
		}

		public void playerAudioTrackCreated(AudioTrack atrack) {
		}
	};

	public static void start() {
		audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		stop();
		runTask();
		isStarted = true;
		txtStatus.setText("Buffering");
		progressBar.setVisibility(View.VISIBLE);
		btnPlay.setVisibility(View.GONE);
		// we cannot do it in playerStarted() - it is too late:
		// txtMetaTitle.setText("");
		// txtMetaGenre.setText("");
		// txtMetaUrl.setText("");
		multiPlayer = new MultiPlayer(mPlayerCallback, 1500, 700);
		// http://kantipur-stream.softnep.com:7248/
		// http://108.163.197.114:8103/
		multiPlayer.playAsync(URL_RADIO_SOFTNEP);
	}

	public static void stop() {
		isStarted = false;
		if (multiPlayer != null) {
			multiPlayer.stop();
			multiPlayer = null;
		}
	}

	

	@SuppressLint("NewApi")
	private static void setNotification() {
		
		Context context = MainActivity.context;

		notification_view = new RemoteViews(context.getPackageName(),
				R.layout.notification_view);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				MainActivity.context);

		builder.setTicker("Now Playing\n" + context.getResources().getString(R.string.radio_name));
		builder.setSmallIcon(R.drawable.ic_launcher);
		
		// This intent is fired when notification is clicked
		Intent intent = new Intent(context, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				intent, 0);

		// Set the intent that will fire when the user taps the notification.
		builder.setContentIntent(pendingIntent);
		// Large icon appears on the left of the notification
		 builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
		 R.drawable.ic_launcher));

		Intent buttonIntent = new Intent(
				"com.softnep.webradio.ACTION_CLOSE");
		buttonIntent.putExtra("notificationId", 1);
		PendingIntent cancelNotifIntent = PendingIntent.getBroadcast(context,
				0, buttonIntent, 0);
		notification_view.setOnClickPendingIntent(R.id.iv_close,
				cancelNotifIntent);
		builder.setContent(notification_view);
		
		mNotificationManager = (NotificationManager) context
				.getSystemService(NOTIFICATION_SERVICE);

		// Will display the notification in the notification bar
		notification = builder.build();
		notification.flags = Notification.FLAG_ONGOING_EVENT;

		mNotificationManager.notify(1, notification);
	
	
	}

	private static void removeNotification() {
		if (mNotificationManager != null)
			mNotificationManager.cancel(1);
	}

}
