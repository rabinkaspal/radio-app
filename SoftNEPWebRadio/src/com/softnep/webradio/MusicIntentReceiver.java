package com.softnep.webradio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MusicIntentReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(
				android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
			// signal your service to stop playback
				if (MainActivity.playerStarted) {
					MainActivity.stop();
					Toast.makeText(context, "Headphones unplugged", Toast.LENGTH_SHORT).show();
				}
		}
	}

}
