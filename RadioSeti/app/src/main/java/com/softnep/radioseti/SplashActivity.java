package com.softnep.radioseti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 3000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash);
		

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent i = new Intent(getApplicationContext(),
						MainActivity.class);
				startActivity(i);
				finish();
				
			}
		}, 5000);
		
		
		

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}

	public void runTask() {
		if (ConnectionHelper.isNetworkAvailable(getApplicationContext())) {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent i = new Intent(getApplicationContext(),
							MainActivity.class);
					startActivity(i);
					finish();
				}

			}, SPLASH_TIME_OUT);

		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					SplashActivity.this);
			builder.setCancelable(false);
			// builder.setTitle("ERROR !!");
			builder.setMessage("\nNo Internet Connection.");

			builder.setPositiveButton("Retry",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							runTask();
						}
					}).setNegativeButton("Settings",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							startActivity(new Intent(
									Settings.ACTION_WIFI_SETTINGS));
							finish();
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
			Toast.makeText(getApplicationContext(), "Network Unavailable!",
					Toast.LENGTH_SHORT).show();
		}
	}

}
