package com.softnep.radioseti;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;

public class NotificationActionButtonReceiver extends BroadcastReceiver {

	MediaPlayer player;
	NotificationManager mNotificationManager;

	@Override
	public void onReceive(Context context, Intent intent) {

		mNotificationManager = MainActivity.mNotificationManager;

//		RemoteViews remoteView = MainActivity.notification_view;

		String action = intent.getAction();
		int notificationId = intent.getIntExtra("notificationId", 0);

		if (action.equalsIgnoreCase("com.softnep.radioseti.ACTION_CLOSE")) {
				MainActivity.stop();
				mNotificationManager.cancel(notificationId);
				context.sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)); 
				MainActivity.closeActivity();			

		} 
//		else if (action
//				.equalsIgnoreCase("com.softnep.radiostreaming.ACTION_PLAY")) {
//			Toast.makeText(context, "Playing", 100).show();
//			MainActivity.startPlaying();
//
//		} else if (action
//				.equalsIgnoreCase("com.softnep.radiostreaming.ACTION_PAUSE")) {
//			Toast.makeText(context, "Pausing", 100).show();
//			MainActivity.pausePlaying();
//		}

	}

}
